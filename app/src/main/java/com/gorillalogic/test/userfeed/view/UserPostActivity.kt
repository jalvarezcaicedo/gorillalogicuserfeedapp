package com.gorillalogic.test.userfeed.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gorillalogic.test.userfeed.R
import com.gorillalogic.test.userfeed.business.post.UserPostPresenter
import com.gorillalogic.test.userfeed.business.post.UserPostView
import com.gorillalogic.test.userfeed.model.UserFeed
import com.gorillalogic.test.userfeed.utils.Constants
import com.gorillalogic.test.userfeed.utils.Util
import com.gorillalogic.test.userfeed.view.adapter.UserFeedItemsAdapter
import kotlinx.android.synthetic.main.activity_user_posts.*
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class UserPostActivity : AppCompatActivity(), UserPostView {

    private var userPostPresenter: UserPostPresenter? = null
    private lateinit var userFeedData: List<UserFeed>
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_posts)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayShowTitleEnabled(false)
        lblUserPost.setOnClickListener {
            startActivity(Intent(this, CreatePostActivity::class.java))
        }
        userPostPresenter = UserPostPresenter(this)
        userPostPresenter!!.callUserPostService()

        val dateFormat = SimpleDateFormat("EEEE, MMMM d", Locale.getDefault())
        val date = Date(Timestamp(System.currentTimeMillis()).time * 1000)
        lblDate.text = dateFormat.format(date)
        lblGreeting.text = getString(R.string.txt_greetings)
    }

    override fun onResume() {
        super.onResume()
        userPostPresenter = UserPostPresenter(this)
        if (::userFeedData.isInitialized) {
            if (userFeedData.size < 0) {
                userPostPresenter!!.callUserPostService()
            } else {
                userFeedData = Util.getUserFeedItems(
                    getSharedPreferences(
                        Constants.SHARED_PREFERENCES,
                        Context.MODE_PRIVATE
                    ).getString(Constants.POSTS, "")
                )
            }

            if (userFeedData.isNotEmpty())
                showUserFeedItems()
        }


    }

    override fun showUserFeedItems() {
        userFeedData = Util.getUserFeedItems(
            getSharedPreferences(
                Constants.SHARED_PREFERENCES,
                Context.MODE_PRIVATE
            ).getString(Constants.POSTS, "")
        )
        viewManager = LinearLayoutManager(this)
        viewAdapter = UserFeedItemsAdapter(userFeedData)

        recyclerView = findViewById<RecyclerView>(R.id.recyclerUserPost).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

}
