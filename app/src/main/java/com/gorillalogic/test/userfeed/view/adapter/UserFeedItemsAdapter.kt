package com.gorillalogic.test.userfeed.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.gorillalogic.test.userfeed.R
import com.gorillalogic.test.userfeed.model.UserFeed
import java.text.SimpleDateFormat
import java.util.*


class UserFeedItemsAdapter(
    private var userFeedItems: List<UserFeed>
) : RecyclerView.Adapter<UserFeedItemsAdapter.UserFeedItemsViewHolder>() {

    //
    private val dateFormat = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())

    @NonNull
    override fun onCreateViewHolder(@NonNull viewGroup: ViewGroup, i: Int): UserFeedItemsViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.post_item, viewGroup, false)

        return UserFeedItemsViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull userFeedItemsViewHolder: UserFeedItemsViewHolder, i: Int) {

        userFeedItemsViewHolder.lblUser.text =
            userFeedItems[i].first_name.plus(" ").plus(userFeedItems[i].last_name)

        val date = Date(userFeedItems[i].unix_timestamp.toLong() * 1000)
        userFeedItemsViewHolder.lblDate.text = dateFormat.format(date)

        userFeedItemsViewHolder.lblPost.text = userFeedItems[i].post_body
    }

    override fun getItemCount(): Int {
        return userFeedItems.size
    }

    class UserFeedItemsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var lblUser: TextView = itemView.findViewById(R.id.lblUser)
        internal var lblDate: TextView = itemView.findViewById(R.id.lblDate)
        internal var lblPost: TextView = itemView.findViewById(R.id.lblPost)

    }

}