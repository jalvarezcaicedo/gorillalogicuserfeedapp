package com.gorillalogic.test.userfeed.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.gorillalogic.test.userfeed.MyApplication
import com.gorillalogic.test.userfeed.R
import com.gorillalogic.test.userfeed.model.UserFeed
import com.gorillalogic.test.userfeed.utils.Constants
import com.gorillalogic.test.userfeed.utils.Util


class CreatePostActivity : AppCompatActivity() {

    private lateinit var userFeedData: MutableList<UserFeed>
    private lateinit var userPost: UserFeed
    private var imagePath: String? = null
    private val chooseImageReq = 1
    private val limitChars: Int = 150
    private var toolbarShare: Button? = null
    private var lblCharacterCounter: TextView? = null
    private var lblAddPhoto: TextView? = null
    private var txtPost: EditText? = null
    private var txtPostWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            lblCharacterCounter!!.text = s.length.toString().plus("/").plus(limitChars.toString())
        }

        override fun afterTextChanged(s: Editable) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayShowTitleEnabled(false)
        lblCharacterCounter = findViewById(R.id.lblCharacterCounter)
        lblCharacterCounter!!.text = "0/".plus(limitChars.toString())
        lblAddPhoto = findViewById(R.id.lblAddPhoto)
        toolbarShare = findViewById(R.id.toolbarShare)
        txtPost = findViewById(R.id.txtPost)
        txtPost?.run { addTextChangedListener(txtPostWatcher) }

        lblAddPhoto?.run {
            setOnClickListener {
                getImageFromGallery()
            }
        }

        toolbarShare?.run {
            setOnClickListener {
                if (txtPost?.text?.length!! > 1) {
                    userFeedData = Util.getUserFeedItems(
                        getSharedPreferences(
                            Constants.SHARED_PREFERENCES,
                            Context.MODE_PRIVATE
                        ).getString(Constants.POSTS, "")
                    )

                    if (imagePath != null) {
                        userPost = UserFeed(
                            userFeedData.size,
                            getString(R.string.txt_first_name),
                            getString(R.string.txt_last_name),
                            txtPost?.text.toString(),
                            (System.currentTimeMillis() / 1000L).toString(),
                            imagePath.toString()
                        )
                    } else {
                        userPost = UserFeed(
                            userFeedData.size,
                            getString(R.string.txt_first_name),
                            getString(R.string.txt_last_name),
                            txtPost?.text.toString(),
                            (System.currentTimeMillis() / 1000L).toString(),
                            ""
                        )
                    }
                    userFeedData.add(0, userPost)

                    Util.setList(
                        MyApplication.getContext(),
                        Constants.POSTS,
                        userFeedData
                    )

                    finish()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var id: Int = item.itemId

        if (id == android.R.id.home) finish()

        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                chooseImageReq -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor =
                        selectedImage?.let {
                            contentResolver.query(
                                it,
                                filePathColumn,
                                null,
                                null,
                                null
                            )
                        }
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    imagePath = cursor.getString(columnIndex)
                    cursor.close()

                    Toast.makeText(
                        applicationContext,
                        "Info photo added with image path $imagePath",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun getImageFromGallery() {
        var intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        var types = arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, types)
        startActivityForResult(intent, chooseImageReq)
    }
}
