package com.gorillalogic.test.userfeed.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.gorillalogic.test.userfeed.R
import com.gorillalogic.test.userfeed.utils.Constants

class SplashActivity : AppCompatActivity() {

    private var delayHandler: Handler? = null

    private val runnable: Runnable = Runnable {
        if (!isFinishing) {

            val intent = Intent(applicationContext, UserPostActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        delayHandler = Handler()
        delayHandler!!.postDelayed(runnable, Constants.SPLASH_DELAY)

    }

    override fun onDestroy() {
        super.onDestroy()
        if (delayHandler != null) delayHandler!!.removeCallbacks(runnable)
    }
}
