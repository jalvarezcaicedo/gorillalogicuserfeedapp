package com.gorillalogic.test.userfeed.model

import com.google.gson.annotations.SerializedName

data class UserFeed(
    @SerializedName("id") var id: Int,
    @SerializedName("first_name") var first_name: String,
    @SerializedName("last_name") var last_name: String,
    @SerializedName("post_body") var post_body: String,
    @SerializedName("unix_timestamp") var unix_timestamp: String,
    @SerializedName("image") var image: String
) {
    constructor() : this(
        0, "",
        "", "",
        "", ""
    )
}