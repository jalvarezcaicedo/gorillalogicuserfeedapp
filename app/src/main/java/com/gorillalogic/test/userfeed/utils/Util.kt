package com.gorillalogic.test.userfeed.utils

import android.content.Context
import com.google.gson.Gson
import com.gorillalogic.test.userfeed.model.UserFeed
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class Util {
    companion object Factory {
        operator fun set(context: Context, key: String, value: String) {
            context.getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE).edit()
                .putString(key, value).apply()
        }

        fun <T> setList(context: Context, key: String, list: List<T>?) {
            val gson = Gson()
            val json: String
            json = if (list != null || list!!.size > 0) {
                gson.toJson(list)
            } else {
                ""
            }

            Util[context, key] = json
        }

        fun getUserFeedItems(json: String?): MutableList<UserFeed> {
            val userFeedList = ArrayList<UserFeed>()
            try {
                val jsonMainArr = JSONArray(json)
                for (i in 0 until jsonMainArr.length()) {
                    var userFeed = UserFeed()
                    val jsonObject = jsonMainArr.getJSONObject(i)

                    userFeed.id = jsonObject.get("id").toString().toInt()
                    userFeed.first_name = jsonObject.get("first_name").toString()
                    userFeed.last_name = jsonObject.get("last_name").toString()
                    userFeed.post_body = jsonObject.get("post_body").toString()
                    userFeed.unix_timestamp = jsonObject.get("unix_timestamp").toString()
                    userFeed.image = jsonObject.get("image").toString()

                    userFeedList.add(userFeed)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            return userFeedList
        }
    }


}