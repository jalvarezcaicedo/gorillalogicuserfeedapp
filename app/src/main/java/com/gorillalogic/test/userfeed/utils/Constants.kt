package com.gorillalogic.test.userfeed.utils

class Constants {
    companion object {
        const val BASE_URL: String = "http://gl-endpoint.herokuapp.com/"
        const val SHARED_PREFERENCES: String = "app_prefs"
        const val POSTS: String = "posts"
        const val SPLASH_DELAY: Long = 2000L
    }
}