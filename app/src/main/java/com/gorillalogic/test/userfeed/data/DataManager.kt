package com.gorillalogic.test.userfeed.data

import com.gorillalogic.test.userfeed.data.remote.DataService
import com.gorillalogic.test.userfeed.model.UserFeed
import io.reactivex.Observable
import retrofit2.Response

class DataManager {
    fun callUserFeedData(): Observable<Response<List<UserFeed>>> {
        return DataService.getRetrofitInterface().getPostData()
    }
}