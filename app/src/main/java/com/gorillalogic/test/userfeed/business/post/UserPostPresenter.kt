package com.gorillalogic.test.userfeed.business.post

import com.gorillalogic.test.userfeed.MyApplication
import com.gorillalogic.test.userfeed.data.DataManager
import com.gorillalogic.test.userfeed.utils.Constants
import com.gorillalogic.test.userfeed.utils.Util
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class UserPostPresenter(userPostView: UserPostView) {
    private var disposable: CompositeDisposable? = null
    private var dataManager: DataManager? = null
    private var userPostView: UserPostView

    init {
        dataManager = DataManager()
        this.userPostView = userPostView
        if (disposable == null)
            disposable = CompositeDisposable()
    }

    fun callUserPostService() {
        disposable!!.add(
            dataManager?.callUserFeedData()?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeOn(Schedulers.io())?.subscribe(
                    { userPostResponse ->
                        Util.setList(
                            MyApplication.getContext(),
                            Constants.POSTS,
                            userPostResponse.body()
                        )
                        userPostView.showUserFeedItems()
                    },
                    {
                        Util.setList(
                            MyApplication.getContext(),
                            Constants.POSTS,
                            ArrayList<Any>()
                        )
                        userPostView.showUserFeedItems()
                    })!!
        )
    }
}
