package com.gorillalogic.test.userfeed.business.post

interface UserPostView {
    fun showUserFeedItems()
}
